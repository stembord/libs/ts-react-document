import {
  Explanation,
  Question,
  Quiz,
  TrueFalsePrompt
} from "@stembord/react-quiz";
import { default as React } from "react";
import { render } from "react-dom";
import { Container } from "reactstrap";
import {
  Def,
  Description,
  Label,
  Logo,
  Image,
  Objective,
  Ref,
  Section
} from "../../lib";
import image200x200 from "./images/200x200.png";
import image770x400 from "./images/770x400.png";

class Document extends React.Component {
  sectionRef: React.RefObject<Section> = React.createRef();

  componentDidMount() {
    console.log(this.sectionRef.current);
  }

  render() {
    return (
      <Section ref={this.sectionRef} title="Radian and Degree Measure">
        <Logo src={image200x200} />
        <Description>This is a Description</Description>
        <Objective>This is a Objective</Objective>
        <Objective>This is another Objective</Objective>
        <Section title="Using Images">
          <Def
            id="lorem-ipsum"
            name="Lorem Ipsum"
            value="Section 1.10.32 of `de Finibus Bonorum et Malorum`, written by Cicero in 45 BC"
          />
          <h4>
            TL;DR <Ref id="2">Skip to Section 2</Ref>
          </h4>
          <p>
            <Ref id="figure_2">See Figure 2</Ref>
          </p>
          <Label id="figure_1">
            <div className="text-center">
              <Image
	    source={image200x200}
	    alt="This is a placeholder image"
	    caption={
	      <p>
	        This is an example <b>caption!</b>.
	      </p>
	    }
	    centerCaption={true}
	  />
            </div>
          </Label>
          <Section title="Sub Section">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </Section>
        </Section>
        <Section title="This is garbage infomation">
          <p>
            These cases are perfectly simple and easy to distinguish. In a free
            hour, when our power of choice is untrammelled and when nothing
            prevents our being able to do what we like best, every pleasure is
            to be welcomed and every pain avoided. But in certain circumstances
            and owing to the claims of duty or the obligations of business it
            will frequently occur that pleasures have to be repudiated and
            annoyances accepted. The wise man therefore always holds in these
            matters to this principle of selection: he rejects pleasures to
            secure other greater pleasures, or else he endures pains to avoid
            worse pains. <Ref id="lorem-ipsum" />
          </p>
          <Section title="Sub Section 2" hideInOutline>
            <Label id="figure_2">
              <div className="text-center">
                <img src={image770x400} />
              </div>
            </Label>
            <Quiz title="Mathematical Properties">
              <Question>
                <TrueFalsePrompt answer={true} true="Yes" false="No">
                  <p>Is the square root of 2 a real number?</p>
                </TrueFalsePrompt>
                <Explanation>
                  <p>Yes, google it</p>
                </Explanation>
              </Question>
            </Quiz>
          </Section>
        </Section>
      </Section>
    );
  }
}

render(
  <Container>
    <Document />
  </Container>,
  document.getElementById("app")
);
