import * as React from "react";
import { Alert } from "reactstrap";
import { Label } from "./Label";
import { SectionData } from "./Section";

export class DefData {
  section: SectionData;
  index: number;
  id: string;
  name: React.ReactChild;
  value: React.ReactChild;
  show: boolean;

  constructor(
    section: SectionData,
    index: number,
    id: string,
    name: React.ReactChild,
    value: React.ReactChild,
    show: boolean
  ) {
    this.section = section;
    this.index = index;
    this.id = id;
    this.name = name;
    this.value = value;
    this.show = show;
  }

  getAbsoluteId() {
    const id = this.section.getAbsoluteId();
    return (id ? id + "." : "") + (this.index + 1);
  }
}

export interface IDefProps {
  id: string;
  name: React.ReactChild;
  value: React.ReactChild;
  show?: boolean;
}

export class Def extends React.Component<IDefProps> {
  static defaultProps = {
    show: true
  };

  static is(value: any): value is Def {
    return React.isValidElement(value) && value.type === Def;
  }

  render() {
    return null;
  }
}

export interface IInternalDefProps {
  section: SectionData;
  def: DefData;
}

export class InternalDef extends React.Component<IInternalDefProps> {
  render() {
    return (
      <Label id={this.props.def.getAbsoluteId()}>
        <Label id={this.props.def.id}>
          {this.props.def.show === true ? (
            <Alert variant="secondary">
              <h6>{this.props.def.getAbsoluteId()}</h6>
              {this.props.def.name}
              <hr />
              {this.props.def.value}
            </Alert>
          ) : (
            this.props.def.name
          )}
        </Label>
      </Label>
    );
  }
}
