import * as React from "react";

export interface IDescriptionProps {
  children: React.ReactChild;
}

export class Description extends React.Component<IDescriptionProps> {
  static is(value: any): value is Description {
    return React.isValidElement(value) && value.type === Description;
  }

  render() {
    return null;
  }
}
