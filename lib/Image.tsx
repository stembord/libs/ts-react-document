import * as React from "react";

export interface IImageProps {
  source: string;
  alt?: string;
  caption?: React.ReactChild | React.ReactChild[];
  width?: string;
  height?: string;
  centerCaption?: boolean;
}

export class Image extends React.Component<IImageProps> {
  render() {
    return (
      <figure className="figure d-block">
        <img
          src={this.props.source}
          className="figure-img mx-auto d-block"
          width={this.props.width}
          height={this.props.height}
          alt={this.props.alt || ""}
        />
        {this.props.caption ? (
          <figcaption
            className={`figure-caption ${
              this.props.centerCaption ? "text-center" : ""
            }`}
          >
            {this.props.caption}
          </figcaption>
        ) : null}
      </figure>
    );
  }
}
