import * as React from "react";

export interface ILabelProps {
  id: string;
  children?: React.ReactChild | React.ReactChild[];
}

export class Label extends React.Component<ILabelProps> {
  render() {
    return <span id={`label-${this.props.id}`}>{this.props.children}</span>;
  }
}
