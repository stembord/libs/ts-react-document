import * as React from "react";

export interface ILinkProps {
  id: string;
  children?: React.ReactChild | React.ReactChild[];
  onClick?: (e: MouseEvent) => void;
}

export const scrollToLabel = (id: string) => {
  const element = document.getElementById(`label-${id}`) as Element;
  element.scrollIntoView();
};

export class Link extends React.Component<ILinkProps> {
  onClick = (e: any) => {
    e.preventDefault();

    if (this.props.onClick) {
      this.props.onClick(e);
    } else {
      scrollToLabel(this.props.id);
    }
  };
  render() {
    return (
      <a {...this.props} href="" onClick={this.onClick}>
        {this.props.children ? this.props.children : this.props.id}
      </a>
    );
  }
}
