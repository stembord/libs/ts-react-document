import * as React from "react";

export interface ILogoProps {
  src: string;
}

export class Logo extends React.Component<ILogoProps> {
  static is(value: any): value is Logo {
    return React.isValidElement(value) && value.type === Logo;
  }

  render() {
    return null;
  }
}
