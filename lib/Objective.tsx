import * as React from "react";

export interface IObjectiveProps {
  children: React.ReactChild;
}

export class Objective extends React.Component<IObjectiveProps> {
  static is(value: any): value is Objective {
    return React.isValidElement(value) && value.type === Objective;
  }

  render() {
    return null;
  }
}
