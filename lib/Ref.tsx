import Octicon, { Link as LinkIcon } from "@primer/octicons-react";
import * as React from "react";
import { Button, Popover, PopoverBody, PopoverHeader } from "reactstrap";

export interface IRefProps {
  id: string;
  children?: React.ReactChild | React.ReactChild[];
}

export class Ref extends React.Component<IRefProps> {
  static is(value: any): value is Ref {
    return React.isValidElement(value) && value.type === Ref;
  }

  render(): undefined {
    throw new Error("Should never render a Ref like this");
  }
}

export interface IInternalRefProps {
  section: SectionData;
  id: string;
  children?: React.ReactChild | React.ReactChild[];
}

export interface IInternalRefState {
  open: boolean;
}

export class InternalRef extends React.Component<
  IInternalRefProps,
  IInternalRefState
> {
  ref: React.RefObject<Link> = React.createRef();

  constructor(props: IInternalRefProps) {
    super(props);

    this.state = {
      open: false
    };
  }

  toggleDef = () => {
    this.setState({ open: !this.state.open });
  };

  onClickDef = () => {
    scrollToLabel(this.props.id);
  };

  render() {
    const def = this.props.section.getDef(this.props.id);

    if (def) {
      return (
        <>
          <Link id={this.props.id} ref={this.ref} onClick={this.toggleDef}>
            {this.props.children ? this.props.children : def.name}
          </Link>
          <Popover
            placement="auto"
            isOpen={this.state.open}
            target={this.props.id}
            toggle={this.toggleDef}
          >
            <PopoverBody>
              <div>{def.value}</div>
              <Button onClick={this.onClickDef}>
                <Octicon icon={LinkIcon} />
              </Button>
            </PopoverBody>
          </Popover>
        </>
      );
    } else {
      return <Link id={this.props.id}>{this.props.children}</Link>;
    }
  }
}

import { Link, scrollToLabel } from "./Link";
import { SectionData } from "./Section";
