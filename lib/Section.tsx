import Octicon, {
  ListUnordered,
  MortarBoard,
  Question
} from "@primer/octicons-react";
import { InternalQuiz, Quiz, QuizData } from "@stembord/react-quiz";
import * as React from "react";
import { Button, Collapse, Container } from "reactstrap";

export class SectionData {
  title: React.ReactChild;
  hideInOutline: boolean = true;
  children: React.ReactChild | React.ReactChild[];
  parent?: SectionData;
  root: SectionData;
  index?: number;
  logo?: string;
  description?: React.ReactChild;

  sections: SectionData[] = [];
  objectives: React.ReactChild[] = [];
  quizzes: QuizData[] = [];

  private defs: DefData[] = [];
  private defMap: Record<string, DefData> = {};

  constructor(
    title: React.ReactChild,
    hideInOutline: boolean,
    index?: number,
    parent?: SectionData,
    children?: React.ReactChild | React.ReactChild[]
  ) {
    this.title = title;
    this.hideInOutline = hideInOutline;
    this.parent = parent;
    this.root = parent ? parent.root : this;
    this.index = index;
    this.children = this.parse(children) as any;
  }

  addDef(def: DefData) {
    if (this.parent) {
      this.parent.addDef(def);
    }
    this.defs.push(def);
    this.defMap[def.getAbsoluteId()] = def;
    this.defMap[def.id] = def;
    return this;
  }

  getDef(id: string): DefData | undefined {
    const def = this.defMap[id];

    if (!def && this.parent) {
      return this.parent.getDef(id);
    } else {
      return def;
    }
  }

  getAbsoluteId() {
    let parentId = "",
      id = "";

    if (this.parent) {
      parentId = this.parent.getAbsoluteId();
    }
    if (this.index != null) {
      id = `${this.index + 1}`;
    }

    return (parentId ? parentId + "." : "") + id;
  }

  addQuiz(quizData: QuizData) {
    this.root.quizzes.push(quizData);

    if (this.parent && this.root !== this.parent) {
      this.parent.quizzes.push(quizData);
    }

    return this;
  }

  private parse(children?: React.ReactChild | React.ReactChild[]) {
    return toArray(children).reduce(
      (children, child, index) => {
        const newChild = this.parseChild(this, child, index);

        if (newChild) {
          children.push(newChild);
        }

        return children;
      },
      [] as React.ReactChild[]
    );
  }

  private parseChild(
    parent: SectionData,
    child: React.ReactChild,
    index: number
  ) {
    if (Section.is(child)) {
      const section = new SectionData(
        child.props.title,
        child.props.hideInOutline,
        parent.sections.length,
        parent,
        child.props.children
      );
      parent.sections.push(section);
      return <InternalSection key={index} section={section} />;
    } else if (Def.is(child)) {
      const def = new DefData(
        parent,
        index,
        child.props.id,
        child.props.name,
        child.props.value,
        child.props.show
      );
      parent.addDef(def);
      return <InternalDef key={index} section={parent} def={def} />;
    } else if (Ref.is(child)) {
      return (
        <InternalRef
          key={index}
          section={parent}
          id={child.props.id}
          children={child.props.children}
        />
      );
    } else if (Quiz.is(child)) {
      const quizData = QuizData.parse(child.props);
      parent.addQuiz(quizData);
      return <InternalQuiz key={index} quiz={quizData} />;
    } else if (Objective.is(child)) {
      parent.objectives.push(child.props.children);
    } else if (Logo.is(child)) {
      parent.logo = child.props.src;
    } else if (Description.is(child)) {
      parent.description = child.props.children;
    } else if (React.isValidElement(child)) {
      const children = parent.parse((child.props as any).children || []) as any;

      return React.cloneElement(
        child,
        {
          ...child.props,
          key: index
        },
        ...children
      );
    } else {
      return child;
    }
  }
}

export interface ISectionProps {
  title: React.ReactChild;
  hideInOutline?: boolean;
  children?: React.ReactChild | React.ReactChild[];
}

export class Section extends React.PureComponent<ISectionProps> {
  static defaultProps = {
    hideInOutline: false
  };

  static parse(props: ISectionProps, index?: number, parent?: SectionData) {
    return new SectionData(
      props.title,
      props.hideInOutline === true,
      index,
      parent,
      props.children
    );
  }

  static is(value: any): value is Section {
    return React.isValidElement(value) && value.type === Section;
  }

  data: SectionData;

  constructor(props: ISectionProps) {
    super(props);

    this.data = Section.parse(props);
  }

  componentWillReceiveProps(props: ISectionProps) {
    this.data = Section.parse(props);
  }

  render() {
    return <InternalRootSection section={this.data} />;
  }
}

export interface IInternalRootSectionProps {
  section: SectionData;
}

export interface IInternalRootSectionState {
  outlineOpen: boolean;
  quizzesOpen: boolean;
  learningObjectivesOpen: boolean;
}

export class InternalRootSection extends React.Component<
  IInternalRootSectionProps,
  IInternalRootSectionState
> {
  state = {
    outlineOpen: false,
    quizzesOpen: false,
    learningObjectivesOpen: false
  };

  renderOutline(sectionData: SectionData) {
    return (
      <ul>
        {sectionData.sections
          .filter(section => section.hideInOutline === false)
          .map((section, index) => (
            <li key={index}>
              <Link id={section.getAbsoluteId()}>{section.title}</Link>
              {this.renderOutline(section)}
            </li>
          ))}
      </ul>
    );
  }

  toggleOutline = () => {
    this.setState({ outlineOpen: !this.state.outlineOpen });
  };

  toggleQuizzesOpen = () => {
    this.setState({ quizzesOpen: !this.state.quizzesOpen });
  };

  toggleLearningObjectives = () => {
    this.setState({
      learningObjectivesOpen: !this.state.learningObjectivesOpen
    });
  };

  render() {
    return (
      <Container fluid className="mt-5 mb-5">
        <div className="clearfix">
          <h1 id="root">{this.props.section.title}</h1>
          <hr />
          <div className="float-right">
            {this.props.section.sections.length > 0 && (
              <Button
                onClick={this.toggleOutline}
                variant={this.state.outlineOpen ? "primary" : "secondary"}
              >
                <Octicon icon={ListUnordered} />
              </Button>
            )}
            {this.props.section.objectives.length > 0 && (
              <Button
                onClick={this.toggleLearningObjectives}
                variant={
                  this.state.learningObjectivesOpen ? "primary" : "secondary"
                }
              >
                <Octicon icon={MortarBoard} />
              </Button>
            )}
            {this.props.section.quizzes.length > 0 && (
              <Button
                onClick={this.toggleQuizzesOpen}
                variant={this.state.quizzesOpen ? "primary" : "secondary"}
              >
                <Octicon icon={Question} />
              </Button>
            )}
          </div>
        </div>

        {this.props.section.sections.length > 0 && (
          <Collapse isOpen={this.state.outlineOpen}>
            <div className="mt-4 mb-4">
              <Octicon icon={ListUnordered} size={32} />
              {this.renderOutline(this.props.section)}
            </div>
          </Collapse>
        )}
        {this.props.section.objectives.length > 0 && (
          <Collapse isOpen={this.state.learningObjectivesOpen}>
            <div className="mt-4 mb-4">
              <Octicon icon={MortarBoard} size={32} />
              <ul>
                {this.props.section.objectives.map((objective, index) => (
                  <li key={index}>{objective}</li>
                ))}
              </ul>
            </div>
          </Collapse>
        )}
        {this.props.section.quizzes.length > 0 && (
          <Collapse isOpen={this.state.quizzesOpen}>
            <div className="mt-4 mb-4">
              <Octicon icon={Question} size={32} />
              <ul>
                {this.props.section.quizzes.map((quizData, index) => (
                  <li key={index}>{quizData.title}</li>
                ))}
              </ul>
            </div>
          </Collapse>
        )}
        {this.props.section.children}
      </Container>
    );
  }
}

export interface IInternalSectionProps {
  section: SectionData;
}

export class InternalSection extends React.Component<IInternalSectionProps> {
  render() {
    return (
      <div className="mt-4 mb-4">
        <h2 id={`label-${this.props.section.getAbsoluteId()}`}>
          {this.props.section.getAbsoluteId()} - {this.props.section.title}
        </h2>
        <hr />
        {this.props.section.children}
      </div>
    );
  }
}

import { Def, DefData, InternalDef } from "./Def";
import { Description } from "./Description";
import { Link } from "./Link";
import { Logo } from "./Logo";
import { Objective } from "./Objective";
import { InternalRef, Ref } from "./Ref";
import { toArray } from "./utils";
