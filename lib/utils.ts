import { isArray } from "util";

export const toArray = <T>(value?: T | T[]): T[] =>
  isArray(value) ? value : value != null ? [value] : [];
